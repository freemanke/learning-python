# Python 学习笔记

## 开发环境

<<<<<<< HEAD
## 版本管理

使用 anaconda 管理 python 多版本环境，通过 [官方地址](https://www.anaconda.com/products/distribution) 下载安装

```bash
conda create -n lp-3.11.5 python=3.11.5
conda env list
conda activate lp-3.11.5
```

## 开发环境和包管理

使用 poetry 管理虚拟环境和更新管理包，通过官方地址下载安装 [poetry](https://python-poetry.org/docs/)
```bash
poetry shell
poetry install
```


## 软件开发基础
=======
conda 23.7.4 + python 3.11.5
>>>>>>> 4855f19 (重构)

准备环境
```bash
conda create -n python3.11.5 python=3.11.5
conda activate python3.11.5
pip install -r requirement.txt
```

运行测试

<<<<<<< HEAD
一个模块就是一个 `.py` 文件，如果要命令行 `python hello.py` 单独运行模块，则需要在模块文件中添加入口

```python
def hello(name):
    print(name)
    
if __name__ == '__main__':
    hello('Hello World')
```

### 导入包

```python
import app
import json
```

### 导入模块

模块导入包括精确导入和模糊导入

```python
import requests as req
from app import main
from app import *
```

### 字符串插值

```pythgon
name = "freemanke"
age = 23
print("My name is {}, age is {}".format(name, age))
print("My name is {0}, age is {1}".format(name, age))
print("My name is {name}, age is {}".format(name, age))
print(f"{name}")
```

### 强类型参数

python 中可以使用将类或方法中的参数标记为强类型，方便代码编写中使用智能提示

```python
class Student:
    name: str = ""
    def __init__(self, name: str):
        self.name = name

def get_student(name: str) -> Student:
    return Student(name)
```

### None 关键字

None 的类型名称为 NoneType，None 的值既不是 False，也不是 0，也不是空字符串而表示没有值，即空值， None 是 NoneType 这个类型的唯一值，如果函数默认没有返回值，则函数的返回值即为 None

### 编译器解释器

1. CPython 是默认的且使用最广泛的 Python 编译器。它是用 C 语言编写的，并使用 GIL（全局解释器锁），CPython 中的编译步骤包括：解码、令牌化、解析、抽象语法树和编译。
2. PyPy 一个开发用来替代 CPython 的编译器， 使用 RPython 语言开发， 主要优势是性能。

### GIL 

Global Interpreter Lock 全局解释器锁，他和 Python 语言没有什么关系，而是解析器 Cpython 实现中引入的一个概念，为了防止多线程并发执行机器码的互斥锁，确保任意时刻只有一个线程在运作，是一个历史遗留问题。

## 程序是如何运行的

在操作系统上安装了 `python` 后，会在系统中安装一个编译器和一个解释引擎(PVM)，编译器用来将源文件编译成字节码文件，解释引擎用来循环解释执行字节码中的指令。

1. python 编译器将源文件 `main.py` 编译成字节码文件 `main.pyc`
2. python 解释引擎循环执行 `main.pyc` 中的字节码指令

## 命令行应用

```shell
print("Hello World")
python hello.py
```

## 交互界面应用

使用 `pyqt5` 和 `pyqt5-tools` 包来开发基于 QT 的交互界面应用程序

## WEB 应用

使用网络编程或者 `django`  `flask`  `fastapi` 等已有框架来开发

Fastapi

## 后台任务

celery + rabbitmq

## 异步编程

多进程、多线程、并行、asyncio


## 代码样式规范和类型检查

首先在开发机上安装 [pre-commit](https://pre-commit.com/#usage) 工具， Macos `brew install pre-commit` Ubuntu `apt install pre-commit` , 然后再项目下安装 hooks 如果没有配置文件的话需要首先创建 `.pre-commit-config.yml` 配置文件，配置中一般会包含：ruff, isort, black, flake8 三个组件
```shell
pre-commit install
```
- ruff Rust 写的代码规范检查检查工具，用于取代：isort, flake8
- isort 对文件中的 `imports` 进行智能重排和格式化
- black 代码格式化工具，自动对代码进行格式化，确保代码符合 PEP8 规范
- flake8 代码规范检查工具
- mypy 静态类型检查工具
- pytest 提交前执行自动化测试，确保修改没有破坏功能

## 配置管理

1. dynaconf

## 日志

1. logging
2. loguru

## 数据库自动升级和迁移

使用 sqlalchemy 实现 ORM, 使用 alembic 实现数据库的迁移和自动升级

1. 通过 sqlalchemy 构建 ORM 数据对象，参见：app/model
2. 通过 alembic 实现升级
```shell

# 在项目根目录下初始化 `alembic` 环境
# 修改生成的 `alembic.ini` 文件中的数据库连接字符串
# 修改 `env.py` 文件中的 `target_metadata = app.model.models.Base.metadata`
alembic init alembic
alembic revision --autogenerate -m "init db"

# 修改模型，然后更新数据库，然后生成自动升级脚本
alembic upgrade head
alembic revision --autogenerate -m "department remove remark"
```

使用代码实现程序启动时的数据库自动迁移

```python
import os
from alembic.command import upgrade
from alembic.config import Config

def migrate():
    migrations_dir = "../alembic"
    config_file = os.path.join("../", "alembic.ini")
    config = Config(file_=config_file)
    config.set_main_option("script_location", migrations_dir)
    upgrade(config, "head")
```


## WEB 服务器

Web 应用开发完成后需要使用服务器运行， [ASGI](https://asgi.readthedocs.io/en/latest/) 目前主要有三个主流实现：uvicorn, hypercorn, daphne

## 如何单元测试

使用  `pytst` 或者 `unittest` 实现单元测试的运行

```shell
poetry run pytest tests/

```

## 使用 mock 对象来测试服务

参见 `test_department_service.py`


## 单元测试覆盖率报告

```shell
poetry run coverage run -m pytest ./tests
poetry run coverage report -m
poetry run coverage html -d reports
open reports/index.html
```

## 常用包

attrs, psutil, coverage

## 如何打包成独立运行程序

通过 `pyinstaller` 工具，将字节码文件，执行器和其他需要的支持文件捆绑打包成独立运行程序。

```shell
pip install pyinstaller
pyinstaller ./app/main.py
```

## 什么是 IronPython

IronPython 是一个能将 `python` 代码编译成 .NET 字节码的解释器实现，同理 JPython 是能将 python 代码编译成 java 字节码的解释器实现

C# 编写代码

```csharp
using System;
class Hello
{
    static void Main() 
    {
        Console.WriteLine("Hello World");
    }
}
```

IronPython 编写代码

```python
print("Hello World")
```

## 编码规范

### 命名规范

- 包名 全小写下划线分隔
- 模块名 全小写下划线分隔
- 文件名 全小写下划线分隔
- 类名 首字母大写驼峰命名
- 类属性名 全小写下划线分隔
- 类方法名 全小写下划线分隔
- 变量名 全小写下划线分隔
- 文件夹名 全小写下划线分隔
- 代码组织结构


## SimpleITK

官方文档：[https://simpleitk.readthedocs.io/en/master/index.html](https://simpleitk.readthedocs.io/en/master/index.html)

### 基本概念

- World Coordinate System 世界坐标系，用于医学成像仪器的坐标系，坐标元素是连续值的向量表示，默认单位：mm
- Image Coordinate System 图像坐标系，坐标元素是离散值的向量表示
- Origin 世界坐标系下，图像相对于坐标系原点的位置
- Spacing 两个像素之间在世界坐标系下的间隔
- Size 每个维度的大小
- Direction cosine matrix 方向余弦矩阵，世界坐标系坐标轴对应于图像像素矩阵的方向，用一组矩阵空间内的基来标识
- pixel
- voxel
- Resampling 图像重采样
- Registration 是指将不同的医学图像（根据定义，计算机内从像素到meta信息有任意不同之处的两张医学图像都是不同的。在现实中的定义则是两次不同成像过程产生的图像。）进行对齐和匹配。医学图像配准的目标是将两个或多个医学图像进行对齐，使它们共享相同的解剖结构、几何形状和空间定位信息，从而实现它们之间的定量比较、分析和整合。常用的医学图像配准方法包括基于特征的方法、基于强度的方法、基于形变场的方法等。
- Cross-Modality Image Registration 跨模态图像配准，如果两张图象来自于不同原理的成像仪器（例如CT和MRI），这种图像配准技术被称作跨模态图像配准
- Multimodal Image Registration 多模态配准

## Pydicom

对 DICOM 文件进行读写，数据集等操作

官方文档：[https://pydicom.github.io/pydicom/stable/](https://pydicom.github.io/pydicom/stable/)
=======
```bash
./test.sh
```
>>>>>>> 4855f19 (重构)
