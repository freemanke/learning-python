import app.main as main


def test_read_root():
    result = main.home()
    assert result["Hello"] == "learning python"
