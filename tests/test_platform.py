import platform


def test_platform():
    print("\n")
    print(platform.system())
    print(platform.release())
    print(platform.version())
    print(platform.architecture())
    print(platform.processor())
    print(platform.python_version())
    print(platform.python_implementation())
    print(platform.uname())
