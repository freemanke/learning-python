def multiple(n):
    return lambda a: a * n


def test_lambda():
    m = multiple(5)
    assert m(5) == 5 * 5
