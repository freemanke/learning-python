def test_tuple():
    words = ("freeman", "jenny", 12, 40, True, False)
    assert words[0] == "freeman"
    assert words[-1] is False
    assert words.index(12) == 2
    assert words.count(40) == 1

    results = (True, False, False)
    assert all(()) is True
    assert True if all(()) else False
    assert all(results) is False
    assert any(results) is True

    numbers = (1, 0, 2, 3)
    assert len(numbers) == 4
    assert max(numbers) == 3
    assert min(numbers) == 0
    assert sum(numbers) == 6
    assert sorted(numbers) == [0, 1, 2, 3]
    assert tuple(sorted(numbers)) == (0, 1, 2, 3)
    assert list(numbers) == [1, 0, 2, 3]
    assert set(numbers) == {1, 0, 2, 3}
    assert str(numbers) == "(1, 0, 2, 3)"
