from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.decl_api import DeclarativeMeta


class TestOrm:
    base: DeclarativeMeta = declarative_base()
    engine = create_engine(
        "sqlite+pysqlite:///:memory:",
        encoding="utf-8",
        echo=True,
    )
    session = sessionmaker(bind=engine)()

    def test_create_db(self):
        self.base.metadata.create_all(self.engine)

    def test_create(self):
        user = User(name="freeman", password="freeman")
        self.session.add(user)
        assert user.id is None
        self.session.commit()
        assert user.id is not None

    def test_query(self):
        user = (
            self.session.query(User)
            .filter(User.id > 0)
            .filter_by(name="freeman")
            .first()
        )
        assert user is not None
        user = self.session.query(User).filter_by(name="jenny").first()
        assert user is None

        users = self.session.query(User.id, User.name).all()
        print(users)


class User(TestOrm.base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    name = Column(String(128))
    password = Column(String(128))


class Record(TestOrm.base):
    __tablename__ = "records"
    id = Column(Integer, primary_key=True)
    score = Column(Integer)
    remark = Column(String(128))
