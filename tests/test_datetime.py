import datetime


def test_datetime():
    age = datetime.datetime(2013, 1, 1)
    now = datetime.datetime.now()
    now.strftime("%y-%m-%d %H:%M:%S")
    shift = now - age
    print(f"\nnow:{now} \nseconds: {shift.total_seconds()} \ndays: {shift.days}")
