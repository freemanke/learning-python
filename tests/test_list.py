def test_list():
    names = [1, 0, 2, 3]
    assert names[0] == 1
    assert names[-1] == 3

    assert len(names) == 4
    appended_names = list(names)
    appended_names.append(4)
    assert appended_names == names + [4]

    reversed_names = list(names)
    reversed_names.reverse()
    assert reversed_names == [3, 2, 0, 1]

    cleared_names = list(names)
    cleared_names.clear()
    assert cleared_names == []

    assert names.count(1) == 1
    assert names.index(2) == 2

    inserted_names = list(names)
    inserted_names.insert(0, -1)
    assert inserted_names == [-1] + names

    popped_names = list(names)
    popped_names.pop()
    assert popped_names == names[: len(names) - 1]

    sorted_names = list(names)
    sorted_names.sort()
    assert sorted_names == [0, 1, 2, 3]
    sorted_names.sort(reverse=True)
    assert sorted_names == [3, 2, 1, 0]
