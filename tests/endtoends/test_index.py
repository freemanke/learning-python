import http.client


class TestGetIndex:
    def test_index(self):
        conn = http.client.HTTPConnection("home.freemanke.com", port=48880, timeout=10)
        conn.request("GET", "/")
        response = conn.getresponse()
        assert 200 == response.status
        print(response.read().decode())
