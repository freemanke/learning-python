from os import path, remove


class TestFileIO:
    file_name = "names.txt"

    @classmethod
    def setup_class(cls) -> None:
        print("class setup")

    @classmethod
    def teardown_class(cls) -> None:
        print("class teardown")

    def setup_method(self):
        print("method setup")
        if not path.exists(self.file_name):
            with open(path.abspath(self.file_name), "w+") as file:
                assert file.readlines() is not None

    def teardown_method(self) -> None:
        print("method teardown")
        filename = path.abspath(self.file_name)
        if path.isfile(filename):
            remove(filename)

    def test_read(self):
        file_path = path.abspath(self.file_name)
        with open(file_path) as file:
            for line in file.readlines():
                assert line is not None

    def test_write(self):
        file_path = path.abspath(self.file_name)
        with open(file_path, "w") as file:
            file.write("01234567890\n")
            file.writelines(["abc\n", "def\n", "ghi\n"])
            file.writelines(["012\n", "345\n", "678\n"])
            assert file.writable() is True

        with open(file_path) as file:
            assert file.writable() is False
