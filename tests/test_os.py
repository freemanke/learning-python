import datetime
import os


def test_os():
    exit_code = os.system("echo '0'")
    assert exit_code == 0
    print(os.getcwd())
    print(os.getenv("PATH", "").split(":"))
    print(os.getenv("LICENSE", "MIT"))


def test_os_path():
    root_path = "/tmp"
    file_name = "update.zip"
    file_path = os.path.join(root_path, file_name)
    assert file_path == f"{root_path}/{file_name}"

    abs_path = os.path.abspath(file_name)
    assert abs_path == f"{os.getcwd()}/{file_name}"

    assert os.path.dirname(file_path) == root_path
    assert os.path.curdir == os.curdir
    assert os.path.split(file_path)[0] == root_path
    assert os.path.split(file_path)[1] == file_name
    assert os.path.exists("/tmp") is True
    print(
        datetime.datetime.fromtimestamp(os.path.getctime(root_path)).strftime(
            "%Y-%m-%d %H:%M:%S"
        )
    )
    print(
        datetime.datetime.fromtimestamp(os.path.getmtime(root_path)).strftime(
            "%Y-%m-%d %H:%M:%S"
        )
    )
    print(
        datetime.datetime.fromtimestamp(os.path.getatime(root_path)).strftime(
            "%Y-%m-%d %H:%M:%S"
        )
    )

    file_content = "0123456789"
    with open(file_path, "w") as f:
        f.write(file_content)

    assert os.path.getsize(file_path) == len(file_content)
    assert os.path.isfile(root_path) is False
    assert os.path.isfile(file_path) is True
    assert os.path.isdir(root_path) is True
