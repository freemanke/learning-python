import datetime

import psutil


def test_psutil():
    print(psutil.boot_time())
    time = datetime.datetime.fromtimestamp(psutil.boot_time())
    print(time)

    print(f"cpu times: {psutil.cpu_times()}")
    print(f"cpu count: {psutil.cpu_count(logical=True)}")
    print(f"version: {psutil.version_info}")
    print(f"pids: {psutil.pids()}")
    print(f"pids: {psutil.disk_partitions()}")
