import http.client


def test_get():
    conn = http.client.HTTPConnection("freemanke.com", timeout=10)
    conn.request("GET", "/")
    response = conn.getresponse()
    conn.close()

    assert response is not None
