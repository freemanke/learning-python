def test_any():
    assert any([False, False, True]) is True
    assert any((False, False, True)) is True
    assert any((False, True)) is True
    assert any([False, False, False]) is False


def test_all():
    assert all([True, True, True]) is True
    assert all([False, True, True]) is False
    assert all([False, False, False]) is False
