import os
import shutil
import uuid


def test_shutil():
    usage = shutil.disk_usage("/tmp")
    print(f"total: {usage.total/1024.0/1024/1024:.2f} GB")
    print(f"used: {usage.used/1024.0/1024/1024:.2f} GB")
    print(f"free: {usage.free/1024.0/1024/1024:.2f} GB")

    path = f"/tmp/{uuid.uuid1()}"
    sub_path = os.path.join(path, str(uuid.uuid1()))
    print(sub_path)
    os.mkdir(path)
    os.mkdir(sub_path)
    shutil.rmtree(path)
