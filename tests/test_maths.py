from app.others.maths import Mathematics


class TestMathematics:
    math_: Mathematics = Mathematics()

    def test_add(self):
        assert 2 == self.math_.add(1, 1)

    def test_subtract(self):
        assert 1 == self.math_.subtract(2, 1)
        assert -1 == self.math_.subtract(1, 2)
