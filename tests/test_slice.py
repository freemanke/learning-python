# List[start:end:step] 取值为左闭右开区间 [0,9)
def test_slice():
    ages = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert ages[::] == ages
    assert ages[0:] == ages
    assert ages[0::] == ages
    assert ages[0::1] == ages
    assert ages[0:10] == ages
    assert ages[0:10:1] == ages
    assert ages[0:1] == [0]
    assert ages[0:4:2] == [0, 2]
    assert ages[::2] == [0, 2, 4, 6, 8]
    assert ages[1:1] == []
    assert ages[100:1] == []
    assert ages[:0] == []


# 如果 start end 都是负数的时候，是从后往前操作，同样是左开右闭区间，序列是从 -1 开始
def test_slice_negative_start_end():
    ages = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert ages[-11:] == ages
    assert ages[-11::] == ages
    assert ages[-11::1] == ages
    assert ages[:-1] == [0, 1, 2, 3, 4, 5, 6, 7, 8]
    assert ages[:-1:] == [0, 1, 2, 3, 4, 5, 6, 7, 8]
    assert ages[:-1:1] == [0, 1, 2, 3, 4, 5, 6, 7, 8]
    assert ages[-10] == 0
    assert ages[-1] == 9
    assert ages[-2] == 8
    assert ages[-10:-8] == [0, 1]


# 如果 IndexJump 为负数，则使用 reversed list 进行操作
def test_slice_negative_index_jump():
    ages = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert ages[::-1] == list(reversed(ages))
    assert ages[::-2] == [9, 7, 5, 3, 1]
    assert ages[:6:-1] == [9, 8, 7]


def test_slice_concatenate():
    ages = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert ages[0:1] + ages[1:2] == [0, 1]
