from attrs import asdict, define, make_class


@define
class Person:
    name: str = ""
    age: int = 0

    def eat(self, food: str) -> str:
        return f"Person {self.name} eat food {food}"


def test_attrs():
    me = Person(name="me", age=100)
    you = Person(name="me", age=100)
    assert me == you
    assert str(me) == "Person(name='me', age=100)"
    assert asdict(me) == asdict(you)
    assert asdict(me) == {"name": "me", "age": 100}

    She = make_class("She", ["name", "age"])
    she = She("she", 99)
    assert she.name == "she"
    assert she.age == 99
