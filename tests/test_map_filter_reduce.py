from functools import reduce


def test_map():
    numbers = [1, 2, 3]
    map_items = map(lambda x: x * x, numbers)
    assert list(map_items) == [1, 4, 9]


def test_filter():
    numbers = [0, 2, 5, 7, 10]
    assert list(filter(lambda x: x % 5 == 0, numbers)) == [0, 5, 10]


def test_reduce():
    assert reduce(lambda x, y: x + y, [0, 1, 2, 3]) == 6
