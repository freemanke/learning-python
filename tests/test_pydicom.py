import os.path

import matplotlib.pyplot as plt
from pydicom import dcmread


def test_read_dicom():
    tests_path = os.path.dirname(os.path.abspath(__file__))
    fileName = f"{tests_path}/data/P00010003/CN531009-P00010003-46072_001.dcm"
    path = os.path.join(os.path.abspath(os.path.curdir), fileName)
    dc = dcmread(path)
    print(dc)

    img_data = dc.pixel_array
    print(img_data.shape)

    plt.imshow(img_data, cmap=plt.cm.bone)
    plt.ylabel("Y")
    plt.xlabel("X")
