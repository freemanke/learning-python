import base64
import os


def test_encode():
    input_ = "freeman ke".encode("utf-8")
    output = base64.b64encode(input_).decode("utf-8")
    assert "ZnJlZW1hbiBrZQ==" == output


def test_decode():
    input_ = "ZnJlZW1hbiBrZQ==".encode("utf-8")
    output = base64.b64decode(input_).decode("utf-8")
    assert "freeman ke" == output


def test_os():
    assert os.getenv("PATH") is not None
