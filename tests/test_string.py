def test_basic():
    my_work = "a" "b" "c" "d"
    assert "abcd" == my_work
    assert 4 == len(my_work)
    for x in iter([1, 2, 3, 4, 5, 6, 7]):
        print(x)


def test_interpolation():
    name = "freemanke"
    age = 18
    result = "my name is freemanke, age is 18"
    assert f"my name is {name}, age is {age}" == result
    assert "my name is {}, age is {}".format(name, age) == result
    assert "my name is {0}, age is {1}".format(name, age) == result
    assert "abc" == "abc"
