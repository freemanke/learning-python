import numpy as np


def test_numpy():
    array = np.array([0, 1, 2, 3])
    assert array.size == 4
    assert array.shape == (4,)

    array2d = np.array([[0, 1, 2], [3, 4, 5]])
    assert array2d.size == 6
    assert array2d.shape == (
        2,
        3,
    )
    assert array2d.ndim == 2

    array = np.zeros((2, 2))
    print(array)
