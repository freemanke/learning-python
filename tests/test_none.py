from types import NoneType


def test_none():
    assert None is NoneType()
    assert str(type(None)) == "<class 'NoneType'>"
    assert None is not False
    assert None is not True
    assert None is not str("")
    assert None is not str()
    assert None is not int(0)
    assert print("") is None

    none_value = None
    assert none_value is None
