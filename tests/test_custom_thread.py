from app.others.custom_thread import CustomThread


class TestCustomThread:
    @staticmethod
    def test_run():
        t = CustomThread("first thread", 1)
        t.run()
        assert 0 == t.counter
