import time


# 装饰器本身就是一个函数，输入参数为函数，内部可是实现在调用这个输入的函数前后执行相关的操作，
# 可以用于面向切面编程实现在不修改原有函数的前提下为其添加新的功能，不如输出日志，输出调用时间等
def log_func_name(func):
    def print_func_name(*args, **kwargs):
        print(f"函数调用开始: {func.__name__}")
        value = func(*args, **kwargs)
        print(f"函数调用结束: {func.__name__}")

        return value

    return print_func_name


def log_call_elapsed(func):
    def call_elapsed(*args, **kwargs):
        begin = time.time()
        value = func(*args, **kwargs)
        end = time.time()
        print(f"函数调用耗时: {(end -begin)*1000:.3f} ms")
        return value

    return call_elapsed


@log_call_elapsed
@log_func_name
def greeting(message: str):
    return message


def test_greeting():
    print("\n")
    print(greeting("Hello there!"))
