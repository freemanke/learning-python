def test_dict():
    users = {"james": 30, "jim": 23, "luce": 39}
    assert users["jim"] == 23
    updated_users = dict(users)
    updated_users.update({"luce": 40})
    assert updated_users["luce"] == 40
    assert list(users.keys()) == ["james", "jim", "luce"]
    assert list(users.values()) == [30, 23, 39]
    assert list(users.items()) == [("james", 30), ("jim", 23), ("luce", 39)]
    assert users.get("james") == 30
    assert users.fromkeys(users.keys(), 0) == {"james": 0, "jim": 0, "luce": 0}
    assert ("james" in users) is True
    assert ("abc" in users) is False
