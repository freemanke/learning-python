FROM duffn/python-poetry:3.10.9-slim-1.4.1-2023-03-24
WORKDIR /app
COPY . /app/
RUN poetry install --no-interaction --no-ansi
EXPOSE 80

CMD ["poetry", "run", "uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]
