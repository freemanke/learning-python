docker pull home.freemanke.com:60013/freemanke/learning-python:latest
docker stop learning-python
docker rm -f learning-python
docker run -d --restart always \
--name learning-python \
-p 48880:80 \
home.freemanke.com:60013/freemanke/learning-python:latest \
poetry run uvicorn app.main:app --host 0.0.0.0 --port 80
docker ps | grep learning-python