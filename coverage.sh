python -m coverage run -m pytest --disable-warnings --ignore=tests/manual ./tests
python -m coverage report -m
python -m coverage html --omit="__init__.py,tests/*,dispatch_agent/protos_output/*"  -d reports
if [ -f "/usr/bin/open" ]; then
open reports/index.html
fi