import logging
import sys
from logging import Formatter, Logger, StreamHandler
from logging.handlers import TimedRotatingFileHandler


class LogService:
    def __init__(self):
        self.formatter = Formatter(
            "%(asctime)s | %(levelname)-8s | %(name)s:%(lineno)03d | %(message)s"
        )
        self.log_file = "/tmp/log.txt"
        self.loggers: dict[str, Logger] = {}

    def get(self, name: str = "default") -> Logger:
        if name not in self.loggers.keys():
            logger = logging.getLogger(name)
            logger.setLevel(logging.DEBUG)
            logger.addHandler(self._get_console_handler())
            logger.addHandler(self._get_file_handler())
            self.loggers.update({name: logger})
        return self.loggers[name]

    def _get_console_handler(self):
        handler = StreamHandler(sys.stdout)
        handler.setFormatter(self.formatter)
        return handler

    def _get_file_handler(self):
        handler = TimedRotatingFileHandler(self.log_file, when="midnight")
        handler.setFormatter(self.formatter)
        return handler
