import uvicorn
from fastapi import FastAPI

from app.api import department, employee
from app.model.database import EnterpriseDbContext
from app.schema.dtos import UserInfo
from common.log import LogService

app = FastAPI()
logger = LogService().get(__name__)

app.include_router(
    department.router,
    prefix="/api/v1/departments",
    tags=["departments"],
)

app.include_router(
    employee.router,
    prefix="/api/v1/employees",
    tags=["employees"],
)


@app.get("/")
def home():
    result = {"Hello": "learning python"}
    return result


@app.get("/api/v1/user", response_model=UserInfo)
async def me():
    result = UserInfo("Freeman", "ke", 20)
    return result


if __name__ == "__main__":
    EnterpriseDbContext().migrate()
    uvicorn.run(app, host="0.0.0.0", port=80)
