from fastapi import APIRouter

from app.schema.dtos import DepartmentInfo
from app.service.department_service import DepartmentService
from common.log import LogService

router = APIRouter()
logger = LogService().get(__name__)
department_service = DepartmentService()


@router.get("/", response_model=list[DepartmentInfo])
def get_all():
    return department_service.get_all()


@router.get("/{department_id}", response_model=DepartmentInfo)
def get_by_id(department_id: int):
    return department_service.get_by_id(department_id)
