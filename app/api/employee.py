from fastapi import APIRouter

from app.schema.dtos import EmployeeInfo
from app.service.employee_service import EmployeeService
from common.log import LogService

router = APIRouter()
logger = LogService().get(__name__)
employee_service = EmployeeService()


@router.get("/", response_model=list[EmployeeInfo])
async def get_all():
    return employee_service.get_all()


@router.get("/{employ_id}", response_model=EmployeeInfo | None)
def get_by_id(employ_id: int):
    return employee_service.get_by_id(employ_id)
