def consumer():
    r = ""
    while True:
        n = yield r
        if n == "":
            return
        print(f"Consumer: {n}")
        r = "OK"


def producer(generator_consumer):
    generator_consumer.send(None)
    n = 0
    while n < 5:
        n = n + 1
        print(f"Producer: {n}")
        r = generator_consumer.send(n)
        print(f"Producer: Consumer return {r}")
    generator_consumer.close()


if __name__ == "__main__":
    c = consumer()
    print(type(c))
    producer(c)
