import threading
import time


class CustomThread(threading.Thread):
    def __init__(self, name, counter):
        threading.Thread.__init__(self)
        self.name = name
        self.counter = counter

    def run(self):
        while self.counter:
            print("{}: {} \r".format(self.name, time.time()))
            self.counter = self.counter - 1
            time.sleep(1)
            if self.counter <= 0:
                break
