
from app.model.database import Employee, EnterpriseDbContext
from app.schema.dtos import EmployeeInfo


class EmployeeService:
    def __init__(self):
        self.session = EnterpriseDbContext().session
       # self.mapper = ObjectMapper()
        #self.mapper.create_map(Employee, EmployeeInfo)
        #self.mapper.create_map(list[Employee], list[EmployeeInfo])

    def get_all(self) -> list[EmployeeInfo]:
        items = self.session.query(Employee).all()
        response = self.mapper.map(items, list[EmployeeInfo])
        return response

    def get_by_id(self, employee_id: int) -> EmployeeInfo | None:
        first = (
            self.session.query(EmployeeInfo)
            .filter(EmployeeInfo.employee_id == employee_id)
            .first()
        )
        if first is not None:
            return self.mapper.map(first, EmployeeInfo)
        else:
            return None
