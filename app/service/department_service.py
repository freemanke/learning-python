
from app.model.database import Department, EnterpriseDbContext
from app.schema.dtos import DepartmentInfo


class DepartmentService:
    def __init__(self):
        self.session = EnterpriseDbContext().session
       # self.mapper = ObjectMapper()
       # self.mapper.create_map(Department, DepartmentInfo)
       # self.mapper.create_map(list[Department], list[DepartmentInfo])

    def get_all(self) -> list[Department]:
        return self.session.query(Department).all()

    def get_by_id(self, department_id: int) -> DepartmentInfo | None:
        first = (
            self.session.query(Department)
            .filter(Department.department_id == department_id)
            .first()
        )
        if first is not None:
            return DepartmentInfo(department_id=first.department_id, name=first.name)
        else:
            return None
