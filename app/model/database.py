import os

from sqlalchemy import Column, DateTime, Integer, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session, sessionmaker
from sqlalchemy.orm.decl_api import DeclarativeMeta

from alembic.command import upgrade
from alembic.config import Config

connection_string = (
    "mysql+mysqlconnector://root:18612257045@home.freemanke.com:34306/learning-python"
)
engine = create_engine(connection_string)
Base: DeclarativeMeta = declarative_base(engine)


class Department(Base):
    __tablename__ = "departments"
    department_id = Column(Integer, primary_key=True)
    name = Column(String(256))


class Employee(Base):
    __tablename__ = "employees"
    employee_id = Column(Integer, primary_key=True)
    name = Column(String(32))
    remark = Column(String(32))
    birthday = Column(DateTime)


class EnterpriseDbContext:
    def __init__(self):
        self.session: Session = sessionmaker(bind=engine)()
        self.base = declarative_base(engine)()

    def add(self, entity):
        self.session.add(entity)
        self.session.commit()

    def close(self):
        self.session.close()

    def drop_all_tables(self):
        self.base.metadata.drop_all()

    def create_all_tables(self):
        self.base.metadata.create_all()

    @staticmethod
    def migrate():
        migrations_dir = "../alembic"
        config_file = os.path.join("../", "alembic.ini")
        config = Config(file_=config_file)
        config.set_main_option("script_location", migrations_dir)
        upgrade(config, "head")
