from pydantic import BaseModel


class UserInfo(BaseModel):
    first_name: str = ""
    last_name: str = ""
    age: int = 0


class DepartmentInfo(BaseModel):
    department_id: int = 0
    name: str = ""


class EmployeeInfo(BaseModel):
    employee_id: str = ""
    name: str = ""
    remark: str = ""
